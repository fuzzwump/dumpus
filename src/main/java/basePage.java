import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class basePage extends baseTest{

    private static final int timeout = 30;

    protected WebDriver driver;
    private WebDriverWait wait;

    public basePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, timeout);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, timeout), this);
    }

    protected void waitForElementPresent(By by) {
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }

    protected void waitForElementNotPresent(By by) {
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }

    protected void waitForTextToDisappear(By by, String text) {
        wait.until(ExpectedConditions.not(ExpectedConditions.textToBe(by, text)));
    }

}
